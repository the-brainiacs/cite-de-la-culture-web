<?php

namespace PiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Salle
 *
 * @ORM\Table(name="salle")
 * @ORM\Entity(repositoryClass="PiBundle\Repository\SalleRepository")
 */
class Salle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     *
     * @Assert\Length(min=5,minMessage="Le nom doit avoir au minimum 5 lettres")
     * @Assert\Length(max=20,maxMessage="Le nom doit avoir au maximum 5 lettres")
     */
    private $nom;

    /**
     * @var int
     *
     * @ORM\Column(name="capacite", type="integer")
     *
     * @Assert\Range(min=10,minMessage="La capacité doit être supérieure à 10")
     * @Assert\Range(max=1000,maxMessage="La capacité doit être inférieure à 1000")
     */
    private $capacite;

    /**
     * @var float
     *
     * @ORM\Column(name="cout_reservation_heure", type="float")
     *
     * @Assert\Range(minMessage="Le coût de réservation doit être supérieure à 10",min=10.0)
     * @Assert\Range(maxMessage="Le coût de réservation doit être inférieure à 200",max=200.0)
     */
    private $cout_reservation_heure;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @return float
     */
    public function getCoutReservationHeure()
    {
        return $this->cout_reservation_heure;
    }

    /**
     * @param float $cout_reservation_heure
     */
    public function setCoutReservationHeure($cout_reservation_heure)
    {
        $this->cout_reservation_heure = $cout_reservation_heure;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Salle
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set capacite
     *
     * @param integer $capacite
     *
     * @return Salle
     */
    public function setCapacite($capacite)
    {
        $this->capacite = $capacite;

        return $this;
    }

    /**
     * Get capacite
     *
     * @return int
     */
    public function getCapacite()
    {
        return $this->capacite;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


}

