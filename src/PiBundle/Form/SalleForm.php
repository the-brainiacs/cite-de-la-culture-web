<?php
/**
 * Created by PhpStorm.
 * User: missaoui
 * Date: 04/04/2019
 * Time: 07:24
 */

namespace PiBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SalleForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom',TextType::class,array('attr' => array(
            'placeholder' => 'Entrer nom',
            '' => '',
        )))
                ->add('type', ChoiceType::class, [
                    'choices'  => [
                        'film' => 'film',
                        'theatre' => 'theatre',
                        'formation' => 'formation',
                        'festival' => 'festival',
                        'musique' => 'musique',
                        'expose' => 'expose',
                    ],
                    'placeholder' => 'Sélectionner un type',
                ])
                ->add('capacite',IntegerType::class)
                ->add('coutReservationHeure',IntegerType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiBundle\Entity\Salle'
        ));
    }

    public function getName()
    {
        return 'pi_bundle_salle_form';
    }

    public function getBlockPrefix()
    {
        return 'pi_bundle_salle_form';
    }
}