<?php
/**
 * Created by PhpStorm.
 * User: missaoui
 * Date: 22/03/2019
 * Time: 15:29
 */

namespace PiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\DateTime;

class EvenementForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("nom",TextType::class,array('attr' => array(
            'placeholder' => 'Entrer nom',
        )))
            ->add("age", ChoiceType::class, [
                'choices'  => [
                    'Interdit au moins de 12 ans' => 'interdit_12_ans',
                    'Tout public' => 'tout_public',
                    'Pour enfants' => 'pour_enfants',
                ],
                'placeholder'=>'Sélectionner un age',
            ])
            ->add('prix',IntegerType::class)
            ->add('prixEtudiants',IntegerType::class)
            ->add("description",TextAreaType::class,array('attr' => array(
                'placeholder' => 'Entrer description'
            )))
            ->add("Img",FileType::class,array('label'=>'Image','data_class' => null,'required'=>false));
        $builder->add("date_debut", DateTimeType::class,array(
            'widget' => 'single_text',
            'input' => 'datetime',
        ));
        $builder->add("date_fin", DateTimeType::class,array(
            'widget' => 'single_text',
            'input' => 'datetime',
        ));
        $builder->add('disponibles',IntegerType::class);
        $builder->add("type", ChoiceType::class, [
            'choices'  => [
                'Sélectionner un type' =>'',
                'film' => 'film',
                'theatre' => 'theatre',
                'formation' => 'formation',
                'festival' => 'festival',
                'musique' => 'musique',
                'expose' => 'expose',
            ],
        ]);
        $builder->add('Salle', EntityType::class, array(
            'required' => true,
            'placeholder' => "Insérer les valeurs nécessaires d'abord",
            'class' => 'PiBundle:Salle',
            'choice_label'=>'nom',
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiBundle\Entity\Evenement'
        ));
    }

    public function getName()
    {
        return 'pi_bundle_evenement_form';
    }

    public function getBlockPrefix()
    {
        return 'pi_bundle_evenement_form';
    }
}