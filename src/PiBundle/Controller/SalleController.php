<?php
/**
 * Created by PhpStorm.
 * User: missaoui
 * Date: 04/04/2019
 * Time: 07:16
 */

namespace PiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use PiBundle\Form\SalleForm;
use PiBundle\Entity\Salle;

class SalleController extends Controller
{
    public function ajouterSalleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $salle = new Salle();
        $form = $this->createForm(SalleForm::class,$salle);
        $form->handleRequest($request);
        if($request->isMethod("post")&&$form->isValid())
        {
            $em->persist($salle);
            $em->flush();
            return $this->redirectToRoute('afficherSalles');
        }
        return $this->render('@Pi/Salle/ajouterSalle.html.twig', array('form'=>$form->createView()));
    }

    public function modifierSalleAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $salle = $em->getRepository("PiBundle:Salle")->find($id);
        $form = $this->createForm(SalleForm::class,$salle);
        $form->handleRequest($request)->isValid();
        if($request->isMethod("post")&&$form->isValid())
        {
            $em->flush();
            return $this->redirectToRoute('afficherSalles');
        }
        return $this->render('@Pi/Salle/modifierSalle.html.twig', array('form'=>$form->createView()));
    }

    public function afficherSallesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $salles = $em->getRepository("PiBundle:Salle")->findAll();
        return $this->render('@Pi/Salle/afficherSalles.html.twig', array("salles"=>$salles));
    }

    public function afficherSalleAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $salle = $em->getRepository("PiBundle:Salle")->find($id);
        $events = $em->getRepository('PiBundle:Evenement')->findBy(array('Salle'=>$salle,'etat'=>'valide'));
        return $this->render('@Pi/Salle/afficherSalle.html.twig', array('salle'=>$salle,'events'=>$events));
    }
}