<?php

namespace PiBundle\Controller;

use AppBundle\Entity\User;
use blackknight467\StarRatingBundle\Form\RatingType;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\Histogram;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use PiBundle\Entity\EvenementEvaluation;
use PiBundle\Entity\Expose;
use PiBundle\Entity\Festival;
use PiBundle\Entity\Film;
use PiBundle\Entity\Formation;
use PiBundle\Entity\Musique;
use PiBundle\Entity\Theatre;
use PiBundle\Repository\SalleRepository;
use PiBundle\Entity\Evenement;
use PiBundle\Entity\Salle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use PiBundle\Form\EvenementForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class EvenementController extends Controller
{
    public function listSallesofEvenementAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $salleRepository = $em->getRepository("PiBundle:Salle");

        $salles = $salleRepository->findSalleDisponibles(date_create_from_format('Y-m-d H:i', $request->query->get("dateDebut")),date_create_from_format('Y-m-d H:i', $request->query->get("dateFin")),$request->query->get("type"),$request->query->get("disponibles"));

        $responseArray = array();
        foreach($salles as $salle){
            $responseArray[] = array(
                "id" => $salle->getId(),
                "nom" => $salle->getNom(),
                "cout_reservation_heure" => $salle->getCoutReservationHeure(),
                'capacite' => $salle->getCapacite(),
                'type' => $salle->getType(),
            );
        }
        return new JsonResponse($responseArray);
    }

    public function listEvenementsAValiderAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evRepository = $em->getRepository("PiBundle:Evenement");
        $ev = $em->getRepository("PiBundle:Evenement")->find($request->get('id'));
        $ev->setEtat('valide');
        $em->flush();
        $events = $evRepository->findBy(array('etat'=>'en_attente_de_validation'));

        $responseArray = array();
        foreach($events as $evv){
            $responseArray[] = array(
                "id" => $evv->getId(),
                "nom" => $evv->getNom(),
                "description" => $evv->getDescription(),
                "type" => $evv->getType(),
                "etat" => $evv->getEtat(),
                "dateDebut" => $evv->getDateDebut()->format('F d, Y H:i'),
                "dateFin" => $evv->getDateFin()->format('F d, Y H:i'),
                "prix" => $evv->getPrix(),
                "prixEtudiants" => $evv->getPrixEtudiants(),
                "salle" => $evv->getSalle()->getNom(),
                "age" => $evv->getAge(),
                "img" =>$evv->getImg()
            );
        }
        return new JsonResponse($responseArray);
    }

    public function listEvenementsARefuserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evRepository = $em->getRepository("PiBundle:Evenement");
        $ev = $em->getRepository("PiBundle:Evenement")->find($request->get('id'));
        $ev->setEtat('refuse');
        $em->flush();
        $events = $evRepository->findBy(array('etat'=>'en_attente_de_validation'));

        $responseArray = array();
        foreach($events as $evv){
            $responseArray[] = array(
                "id" => $evv->getId(),
                "nom" => $evv->getNom(),
                "description" => $evv->getDescription(),
                "type" => $evv->getType(),
                "etat" => $evv->getEtat(),
                "dateDebut" => $evv->getDateDebut()->format('F d, Y H:i'),
                "dateFin" => $evv->getDateFin()->format('F d, Y H:i'),
                "prix" => $evv->getPrix(),
                "prixEtudiants" => $evv->getPrixEtudiants(),
                "salle" => $evv->getSalle()->getNom(),
                "age" => $evv->getAge(),
                "img" =>$evv->getImg()
            );
        }
        return new JsonResponse($responseArray);
    }

    public function listEvenementsARechercherAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evRepository = $em->getRepository("PiBundle:Evenement");
        $events = $evRepository->recherche($request->get('rech'));

        $responseArray = array();
        foreach($events as $evv){
            $responseArray[] = array(
                "id" => $evv->getId(),
                "nom" => $evv->getNom(),
                "description" => $evv->getDescription(),
                "type" => $evv->getType(),
                "etat" => $evv->getEtat(),
                "dateDebut" => $evv->getDateDebut()->format('M d, Y H:i'),
                "dateFin" => $evv->getDateFin()->format('M d, Y H:i'),
                "prix" => $evv->getPrix(),
                "prixEtudiants" => $evv->getPrixEtudiants(),
                "salle" => $evv->getSalle()->getNom(),
                "age" => $evv->getAge(),
                "img" =>$evv->getImg()
            );
        }
        return new JsonResponse($responseArray);
    }

    public function ajouterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = new Evenement();
        $form = $this->createForm(EvenementForm::class,$evenement);
        $form->handleRequest($request);
        if($request->isMethod("post")&&$form->isValid())
        {
            $eventadd = null;
            if($evenement->getType()=="expose")
            {
                $eventadd = new Expose();
                $eventadd->setLocuteurs($request->get('locuteurs'));
                $eventadd->setTheme($request->get('themeExpose'));
            }
            if($evenement->getType()=="musique")
            {
                $eventadd = new Musique();
                $eventadd->setArtistes($request->get('artistes'));
                $eventadd->setGenre($request->get('genreMusique'));
            }
            if($evenement->getType()=="formation")
            {
                $eventadd = new Formation();
                $eventadd->setLocuteurs($request->get('locuteurs'));
                $eventadd->setTheme($request->get('themeFormation'));
            }
            if($evenement->getType()=="festival")
            {
                $eventadd = new Festival();
                $eventadd->setGenreFestival($request->get('genreFestival'));
                $eventadd->setParticipants($request->get('participants'));
                $eventadd->setTypeFestival($request->get('typeFestival'));
            }
            if($evenement->getType()=="theatre")
            {
                $eventadd = new Theatre();
                $eventadd->setArtistes($request->get('artistes'));
                $eventadd->setRealisateurs($request->get('realisateurs'));
                $eventadd->setAnneeSortie(\DateTime::createFromFormat('Y-m-d',$request->get('anneeSortie')));
                $eventadd->setAuteurs($request->get('auteurs'));
                $eventadd->setGenre($request->get('genre'));
            }
            if($evenement->getType()=="film")
            {
                $eventadd = new Film();
                $eventadd->setActeurs($request->get('acteurs'));
                $eventadd->setRealisateurs($request->get('realisateurs'));
                $eventadd->setAnneeSortie(\DateTime::createFromFormat('Y-m-d',$request->get('anneeSortie')));
                $eventadd->setDuree($request->get('duree'));
                $eventadd->setGenre($request->get('genre'));
            }
            $cr = $evenement->getSalle();
            $interval = strtotime($evenement->getDateFin()->format('m/d/y H:i').':00')-strtotime($evenement->getDateDebut()->format('m/d/y H:i').':00');
            $evenement->setCoutReservation($cr->getCoutReservationHeure()*$interval/3600);
            $evenement->setEtat('en_attente_de_validation');

            $conn = ftp_connect("localhost","54218") or die ("Cannot initiate connection to host");
            ftp_login($conn, "brainiacs", "") or die("Cannot login");
            ftp_put($conn, str_replace(' ', '', $evenement->getNom()), $evenement->getImg(), FTP_BINARY);
            ftp_close($conn);
            $evenement->setImg(str_replace(' ', '', $evenement->getNom()));
            $user=$em->getRepository('AppBundle:User')->find(2);
            $evenement->setUser($user);

            $em->persist($evenement);
            $em->flush();
            $eventadd->setId($evenement->getId());
            $em->persist($eventadd);
            $em->flush();
            return $this->redirectToRoute('afficherOrganisateur');
        }
        return $this->render('@Pi/Evenement/ajouter.html.twig', array('form'=>$form->createView()));
    }

    public function afficherAction()
    {
        if(!empty($this->getUser()))
        {
            $em = $this->getDoctrine()->getManager();
            $rec = $em->getRepository('PiBundle:Evenement')->recommander($this->getUser(),3);
            $events = $em->getRepository("PiBundle:Evenement")->findBy(array('etat'=>"valide"),array('dateDebut'=>'DESC'));
            return $this->render('@Pi/Evenement/afficher.html.twig', array("events"=>$events,'rec'=>$rec));
        }
        $em = $this->getDoctrine()->getManager();
        $rec = null;
        $events = $em->getRepository("PiBundle:Evenement")->findBy(array('etat'=>"valide"),array('dateDebut'=>'DESC'));
        return $this->render('@Pi/Evenement/afficher.html.twig', array("events"=>$events,'rec'=>$rec));
    }

    public function afficherAdminAction()
    {
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository("PiBundle:Evenement")->findBy(array('etat'=>"en_attente_de_validation"),array('dateDebut'=>'ASC'));
        return $this->render('@Pi/Evenement/afficherAdmin.html.twig', array("events"=>$events));
    }

    public function afficherOrganisateurAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $events = $em->getRepository("PiBundle:Evenement")->findBy(array('user'=>$user),array('dateDebut'=>'DESC'));
        return $this->render('@Pi/Evenement/afficherOrganisateur.html.twig', array("events"=>$events));
    }

    public function afficherEvenementAction($id, Request $request, SessionInterface $session)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository("PiBundle:Evenement")->find($id);
        $eventplus = null;
        if($event->getType()=="film")
            $eventplus = $em->getRepository("PiBundle:Film")->find($id);
        if($event->getType()=="theatre")
            $eventplus = $em->getRepository("PiBundle:Theatre")->find($id);
        if($event->getType()=="formation")
            $eventplus = $em->getRepository("PiBundle:Formation")->find($id);
        if($event->getType()=="festival")
            $eventplus = $em->getRepository("PiBundle:Festival")->find($id);
        if($event->getType()=="musique")
            $eventplus = $em->getRepository("PiBundle:Musique")->find($id);
        if($event->getType()=="expose")
            $eventplus = $em->getRepository("PiBundle:Expose")->find($id);

        $user = $this->getUser();
        $noteMoyenne=0;
        $allEvaluations = $em->getRepository('PiBundle:EvenementEvaluation')->findBy(array('Evenement'=>$event));
        if(!empty($allEvaluations))
        {
            foreach ($allEvaluations as $eval)
                $noteMoyenne+=$eval->getNote();
            $noteMoyenne = $noteMoyenne/count($allEvaluations);
        }
        if($user!=$event->getUser())
        {
            $evaluation = $em->getRepository('PiBundle:EvenementEvaluation')->findOneBy(array('Evenement'=>$event,'user'=>$user));
            if(empty($evaluation))
            {
                $evaluation = new EvenementEvaluation();
                $evaluation->setUser($user);
                $evaluation->setEvenement($event);
            }
            $session->set('evev',$evaluation);
            $form = $this->createFormBuilder($evaluation)
                ->add('note', RatingType::class)
                ->getForm();
            $form->handleRequest($request);
            return $this->render('@Pi/Evenement/afficherEvenement.html.twig', array("noteMoyenne"=>$noteMoyenne,"e"=>$event,"ep"=>$eventplus,'form'=>$form->createView()));
        }

        return $this->render('@Pi/Evenement/afficherEvenement.html.twig', array("noteMoyenne"=>$noteMoyenne,"e"=>$event,"ep"=>$eventplus));
    }

    public function afficherEvenementAdminAction($id, Request $request, SessionInterface $session)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository("PiBundle:Evenement")->find($id);
        $eventplus = null;
        if($event->getType()=="film")
            $eventplus = $em->getRepository("PiBundle:Film")->find($id);
        if($event->getType()=="theatre")
            $eventplus = $em->getRepository("PiBundle:Theatre")->find($id);
        if($event->getType()=="formation")
            $eventplus = $em->getRepository("PiBundle:Formation")->find($id);
        if($event->getType()=="festival")
            $eventplus = $em->getRepository("PiBundle:Festival")->find($id);
        if($event->getType()=="musique")
            $eventplus = $em->getRepository("PiBundle:Musique")->find($id);
        if($event->getType()=="expose")
            $eventplus = $em->getRepository("PiBundle:Expose")->find($id);

        $user = $this->getUser();
        $noteMoyenne=0;
        $allEvaluations = $em->getRepository('PiBundle:EvenementEvaluation')->findBy(array('Evenement'=>$event));
        if(!empty($allEvaluations))
        {
            foreach ($allEvaluations as $eval)
                $noteMoyenne+=$eval->getNote();
            $noteMoyenne = $noteMoyenne/count($allEvaluations);
        }
        if($user!=$event->getUser())
        {
            $evaluation = $em->getRepository('PiBundle:EvenementEvaluation')->findOneBy(array('Evenement'=>$event,'user'=>$user));
            if(empty($evaluation))
            {
                $evaluation = new EvenementEvaluation();
                $evaluation->setUser($user);
                $evaluation->setEvenement($event);
            }
            $session->set('evev',$evaluation);
            $form = $this->createFormBuilder($evaluation)
                ->add('note', RatingType::class)
                ->getForm();
            $form->handleRequest($request);
            return $this->render('@Pi/Evenement/afficherEvenementAdmin.html.twig', array("noteMoyenne"=>$noteMoyenne,"e"=>$event,"ep"=>$eventplus,'form'=>$form->createView()));
        }

        return $this->render('@Pi/Evenement/afficherEvenementAdmin.html.twig', array("noteMoyenne"=>$noteMoyenne,"e"=>$event,"ep"=>$eventplus));
    }

    public function evenementEvaluationsAction(Request $request, SessionInterface $session)
    {
        $em = $this->getDoctrine()->getManager();
        $evaluation = $session->get('evev');
        if(empty($evaluation->getId()) || empty($em->getRepository('PiBundle:EvenementEvaluation')->find($evaluation->getId())))
        {
            $evaluation->setEvenement($em->getRepository('PiBundle:Evenement')->find($evaluation->getEvenement()->getId()));
            $evaluation->setUser($em->getRepository('AppBundle:User')->find($evaluation->getUser()->getId()));
            $evaluation->setNote($request->query->get("note"));
            $em->persist($evaluation);
        }
        else {
            $evaluation = $em->getRepository('PiBundle:EvenementEvaluation')->find($evaluation->getId());
            $evaluation->setNote($request->query->get("note"));
        }
        $em->flush();
        $allEvaluations = $em->getRepository('PiBundle:EvenementEvaluation')->findBy(array('Evenement'=>$evaluation->getEvenement()));
        $noteMoyenne=0;
        if(!empty($allEvaluations))
        {
            foreach ($allEvaluations as $eval)
                $noteMoyenne+=$eval->getNote();
            $noteMoyenne = $noteMoyenne/count($allEvaluations);
        }
        return new JsonResponse(array('noteMoyenne' => $noteMoyenne), 200);
    }

    public function modifierEvenementAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $eventadd = null;
        $evenement = $em->getRepository('PiBundle:Evenement')->find($id);
        if($evenement->getType()=="expose")
        {
            $eventadd = $em->getRepository('PiBundle:Expose')->find($id);
        }
        if($evenement->getType()=="musique")
        {
            $eventadd = $em->getRepository('PiBundle:Musique')->find($id);
        }
        if($evenement->getType()=="formation")
        {
            $eventadd = $em->getRepository('PiBundle:Formation')->find($id);
        }
        if($evenement->getType()=="festival")
        {
            $eventadd = $em->getRepository('PiBundle:Festival')->find($id);
        }
        if($evenement->getType()=="theatre")
        {
            $eventadd = $em->getRepository('PiBundle:Theatre')->find($id);
        }
        if($evenement->getType()=="film")
        {
            $eventadd = $em->getRepository('PiBundle:Film')->find($id);
        }
        $img=$evenement->getImg();
        $form = $this->createForm(EvenementForm::class,$evenement);
        $form->handleRequest($request);
        if($request->isMethod("post")&&$form->isValid())
        {
            if($evenement->getType()=="expose")
            {
                $eventadd->setLocuteurs($request->get('locuteurs'));
                $eventadd->setTheme($request->get('themeExpose'));
            }
            if($evenement->getType()=="musique")
            {
                $eventadd->setArtistes($request->get('artistes'));
                $eventadd->setGenre($request->get('genreMusique'));
            }
            if($evenement->getType()=="formation")
            {
                $eventadd->setLocuteurs($request->get('locuteurs'));
                $eventadd->setTheme($request->get('themeFormation'));
            }
            if($evenement->getType()=="festival")
            {
                $eventadd->setGenreFestival($request->get('genreFestival'));
                $eventadd->setParticipants($request->get('participants'));
                $eventadd->setTypeFestival($request->get('typeFestival'));
            }
            if($evenement->getType()=="theatre")
            {
                $eventadd->setArtistes($request->get('artistes'));
                $eventadd->setRealisateurs($request->get('realisateurs'));
                $eventadd->setAnneeSortie(\DateTime::createFromFormat('Y-m-d',$request->get('anneeSortie')));
                $eventadd->setAuteurs($request->get('auteurs'));
                $eventadd->setGenre($request->get('genre'));
            }
            if($evenement->getType()=="film")
            {
                $eventadd->setActeurs($request->get('acteurs'));
                $eventadd->setRealisateurs($request->get('realisateurs'));
                $eventadd->setAnneeSortie(\DateTime::createFromFormat('Y-m-d',$request->get('anneeSortie')));
                $eventadd->setDuree($request->get('duree'));
                $eventadd->setGenre($request->get('genre'));
            }
            if(empty($evenement->getImg()))
            {
                $evenement->setImg($img);
            }
            else{
                $conn = ftp_connect("localhost","54218") or die ("Cannot initiate connection to host");
                ftp_login($conn, "brainiacs", "") or die("Cannot login");
                ftp_put($conn, str_replace(' ', '', $evenement->getNom()), $evenement->getImg(), FTP_BINARY);
                ftp_close($conn);
                $evenement->setImg(str_replace(' ', '', $evenement->getNom()));
            }
            $em->flush();
            return $this->redirectToRoute('afficherEvenement',array('id'=>$evenement->getId()));
        }
        $evenement->setImg($img);
        return $this->render('@Pi/Evenement/modifier.html.twig', array('form'=>$form->createView(),'e'=>$evenement,'ep'=>$eventadd));
    }

    public function supprimerEvenementAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $eventadd = null;
        $evenement = $em->getRepository('PiBundle:Evenement')->find($id);
        if($evenement->getType()=="expose")
        {
            $eventadd = $em->getRepository('PiBundle:Expose')->find($id);
        }
        if($evenement->getType()=="musique")
        {
            $eventadd = $em->getRepository('PiBundle:Musique')->find($id);
        }
        if($evenement->getType()=="formation")
        {
            $eventadd = $em->getRepository('PiBundle:Formation')->find($id);
        }
        if($evenement->getType()=="festival")
        {
            $eventadd = $em->getRepository('PiBundle:Festival')->find($id);
        }
        if($evenement->getType()=="theatre")
        {
            $eventadd = $em->getRepository('PiBundle:Theatre')->find($id);
        }
        if($evenement->getType()=="film")
        {
            $eventadd = $em->getRepository('PiBundle:Film')->find($id);
        }
        $em->remove($eventadd);
        $em->remove($evenement);
        $em->flush();
        return $this->redirectToRoute('afficherOrganisateur');
    }

    public function mesStatsAction()
    {
        $arr=array('Evaluation');
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository('PiBundle:Evenement')->findBy(array('user'=>$this->getUser()));
        $nbr = count($events);
        foreach($events as $ev)
        {
            array_push($arr,$this->moyenneEvaluation($ev));
        }
        $max = max($arr);
        $min = min($arr);
        $evmin = $evmax = null;
        $evmaxmoy = $evminmoy = 0;
        foreach($events as $ev)
        {
            if($this->moyenneEvaluation($ev)==$max)
            {
                $evmax=$ev;
                $evmaxmoy = $this->moyenneEvaluation($ev);
            }
            if($this->moyenneEvaluation($ev)==$min)
            {
                $evmin=$ev;
                $evminmoy = $this->moyenneEvaluation($ev);
            }
        }
        $histogram = new Histogram();
        $histogram->getData()->setArrayToDataTable(
            array_chunk ( $arr , 1,true)
        );
        $histogram->getOptions()->setTitle('Nombre des Evaluations par note moyenne');
        $histogram->getOptions()->setWidth(900);
        $histogram->getOptions()->setHeight(500);
        $histogram->getOptions()->getLegend()->setPosition('none');
        $histogram->getOptions()->getHistogram()->setLastBucketPercentile(5);
        $histogram->getOptions()->getHistogram()->setBucketSize(1);
        $histogram->getOptions()->setColors(['cadetblue']);

        $pieChart = new PieChart();
        $v1 = $v2 = $v3 = $v4 = $v5 = $v6 = 0;
        foreach($events as $ev)
        {
            if($ev->getType()=='film')
                $v1++;
            if($ev->getType()=='theatre')
                $v2++;
            if($ev->getType()=='formation')
                $v3++;
            if($ev->getType()=='festival')
                $v4++;
            if($ev->getType()=='expose')
                $v5++;
            if($ev->getType()=='musique')
                $v6++;
        }
        $pieChart->getData()->setArrayToDataTable(
            [
                ['Type de l\'évènement', 'nombre des évènements'],
                ['Formation', $v3],
                ['Expose', $v5],
                ['Musique', $v6],
                ['Film',  $v1],
                ['Theatre',  $v2],
                ['Festival', $v4],
            ]
        );
        $pieChart->getOptions()->setPieSliceText('label');
        $pieChart->getOptions()->setTitle('Type des évènements');
        $pieChart->getOptions()->setPieStartAngle(100);
        $pieChart->getOptions()->setHeight(400);
        $pieChart->getOptions()->setWidth(600);

        $pieChart2 = new PieChart();
        $v1 = $v2 = $v3 = $v4 = $v5 = $v6 = 0;
        foreach($events as $ev)
        {
            if($ev->getEtat()=='en_attente_de_validation')
                $v1++;
            if($ev->getEtat()=='valide')
                $v2++;
            if($ev->getEtat()=='refuse')
                $v3++;
        }
        $pieChart2->getData()->setArrayToDataTable(
            [
                ['Etat de l\'évènement', 'nombre des évènements'],
                ['Validé',  $v2],
                ['Refusé', $v3],
                ['En attente',  $v1],
            ]
        );
        $pieChart2->getOptions()->setPieSliceText('label');
        $pieChart2->getOptions()->setTitle('Etat des évènements');
        $pieChart2->getOptions()->setPieStartAngle(100);
        $pieChart2->getOptions()->setHeight(400);
        $pieChart2->getOptions()->setWidth(600);

        $col = new ColumnChart();


        return $this->render('@Pi/Evenement/mesStats.html.twig',array('histogram'=>$histogram,'pieChart'=>$pieChart,'pieChart2'=>$pieChart2,'nbr'=>$nbr,'evmax'=>$evmax,'evmin'=>$evmin,'evmaxmoy'=>$evmaxmoy,'evminmoy'=>$evminmoy));
    }

    public function recommanderAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rec = $em->getRepository('PiBundle:Evenement')->recommander($this->getUser(),20);
        return $this->render('@Pi/Evenement/recommander.html.twig', array("events"=>$rec));
    }

    public function validerEvenementAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $ev = $em->getRepository("PiBundle:Evenement")->find($id);
        $ev->setEtat('valide');
        $em->flush();
        return $this->redirectToRoute('afficherAdmin');
    }

    public function refuserEvenementAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $ev = $em->getRepository("PiBundle:Evenement")->find($id);
        $ev->setEtat('refuse');
        $em->flush();
        $events = $em->getRepository("PiBundle:Evenement")->findBy(array('etat'=>"en_attente_de_validation"));
        return $this->redirectToRoute('afficherAdmin');
    }

    private function moyenneEvaluation($ev) {
        $i = 0;
        $em = $this->getDoctrine()->getManager();
        $lee = $em->getRepository('PiBundle:EvenementEvaluation')->findBy(array('Evenement'=>$ev));
        if (!empty($lee)) {
            foreach($lee as $ee) {
                $i += $ee->getNote();
                }
                $i = $i / count($lee);
            }
        return $i;
    }

}
